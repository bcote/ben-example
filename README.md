ATLAS Software Bootcamp
-----------------------

This is my bootcamp project.
There are many others like it but this one is mine.
Without my project, I am useless. Without me, my project is useless.

I will code clearly and simply.
I will document enough that my collegues may follow my work,
but not so excessively as to frighten them with walls of text.

All code will eventually need to die. I will write my code knowing this.
I will make my code modular.
I will introduce few dependencies.
When my project dies the pieces will be recycled by my collegues.
If I code poorly, they will curse me.
If I code well, they will praise me.

Blah blah blah test!
