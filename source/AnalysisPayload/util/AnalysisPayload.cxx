// stdlib functionality
#include <iostream>
#include <fstream>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
// Add a submodule for JetSelectionHelper
#include "JetSelectionHelper/JetSelectionHelper.h"
// jet calibration 
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"


//#include "AthenaKernel/errorcheck.h"

int main(int argc, char** argv) {
  Long64_t numEntries(-1);
  if(argc >= 4) numEntries  = std::atoi(argv[3]);

  int eventcount=0;
  int jetcount=0;
  
  // add jet selection helper
  JetSelectionHelper jet_selector;

  // add jet calibration tool
  asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;
  JetCalibrationTool_handle.setTypeAndName("JetCalibrationTool/MyCalibrationTool");
  JetCalibrationTool_handle.setProperty("JetCollection","AntiKt4EMTopo"                                                  );
  JetCalibrationTool_handle.setProperty("ConfigFile"   ,"JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config");
  JetCalibrationTool_handle.setProperty("CalibSequence","JetArea_Residual_EtaJES_GSC_Smear"                              );
  JetCalibrationTool_handle.setProperty("CalibArea"    ,"00-04-82"                                                       );
  JetCalibrationTool_handle.setProperty("IsData"       ,false                                                            );
  JetCalibrationTool_handle.retrieve();

  // read config file for input parameters
  /*std::ifstream config("/home/atlas/Bootcamp/build/config.txt");
  std::string fill;
  std::string val;
  std::string input_file_path;
  float pt_cut;
  float eta_cut;
  if ( config.is_open() ) {
    while( !config.eof() ) {
      config >> fill >> val;
      if ( fill=="input_file_path" ) {
	input_file_path=val;
      }
      else if ( fill=="pt_cut_GeV" ) {
	pt_cut=std::stof(val);
      }
      else if ( fill=="eta_cut" ) {
	eta_cut=std::stof(val);
      }
      else {
	std::cout<<"Error: Input not a parameter!"<<std::endl;
      }
    }
    config.close();
  }*/

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath;
  TString outputFilePath = "myOutputFile.root";
  if(argc >= 2) inputFilePath = argv[1];
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  if(!iFile) return 1;
  event.readFrom( iFile.get() );

  // make histograms for storage
  TH1D *h_njets_raw = new TH1D("h_njets_raw","",20,0,20);
  TH1D *h_njets_raw_calib = new TH1D("h_njets_raw_calib","",20,0,20);
  TH1D *h_njets_kin = new TH1D("h_njets_kin","",20,0,20);
  TH1D *h_njets_kin_calib = new TH1D("h_njets_kin_calib","",20,0,20);
  TH1D *h_njets_type = new TH1D("h_njets_type","",20,0,20);
  TH1D *h_njets_type_calib = new TH1D("h_njets_type_calib","",20,0,20);
  TH1D *h_njets_comb = new TH1D("h_njets_comb","",20,0,20);
  TH1D *h_njets_comb_calib = new TH1D("h_njets_comb_calib","",20,0,20);

  TH1D *h_mjj_raw = new TH1D("h_mjj_raw","",20,0,500);
  TH1D *h_mjj_raw_calib = new TH1D("h_mjj_raw_calib","",20,0,500);
  TH1D *h_mjj_kin = new TH1D("h_mjj_kin","",20,0,500);
  TH1D *h_mjj_kin_calib = new TH1D("h_mjj_kin_calib","",20,0,500);
  TH1D *h_mjj_type = new TH1D("h_mjj_type","",20,0,500);
  TH1D *h_mjj_type_calib = new TH1D("h_mjj_type_calib","",20,0,500);
  TH1D *h_mjj_comb = new TH1D("h_mjj_comb","",20,0,500);
  TH1D *h_mjj_comb_calib = new TH1D("h_mjj_comb_calib","",20,0,500);

  TH1D *z_mjj = new TH1D("z_mjj","",20,0,500);
  TH1D *mjj_sublead_subsublead = new TH1D("mjj_sublead_subsublead","",20,0,500);
  TH1D *mjj_subsublead_subsubsublead = new TH1D("mjj_subsublead_subsubsublead","",20,0,500);

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  if(numEntries == -1) numEntries = event.getEntries();
  std::cout << "Processing " << numEntries << " events" << std::endl;

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {
    eventcount=eventcount+1;

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    if (!event.retrieve (ei, "EventInfo").isSuccess()) {
      std::cout<<"poop"<<std::endl;
    }
    std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");
    
    jetcount=jetcount+1;
    // make temporary vector of jets for those which pass selection
    std::vector<xAOD::Jet> jets_raw;
    std::vector<xAOD::Jet> jets_raw_calib;
    // make temporary vector of jets for those which pass kinematic cuts
    std::vector<xAOD::Jet> jets_kin;
    std::vector<xAOD::Jet> jets_kin_calib;
    // make temporary vector of jets for those which pass b type requirement
    std::vector<xAOD::Jet> jets_type;
    std::vector<xAOD::Jet> jets_type_calib;
    // make temporary vector of jets for those which pass both kinematic cuts and b type requirement
    std::vector<xAOD::Jet> jets_comb;
    std::vector<xAOD::Jet> jets_comb_calib;

    std::vector<xAOD::Jet> jets_em;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // calibrate the jet
      xAOD::Jet *calibratedjet;
      JetCalibrationTool_handle->calibratedCopy(*jet,calibratedjet);

      // make kinematic cuts for jets
      if( jet_selector.isJetGood(jet) ){
        jets_kin.push_back(*jet);
        // make b type requirement for jets
        if( jet_selector.isJetBFlavor(jet) ){
          jets_comb.push_back(*jet);
        }
      }
   
      // make b type requirement for jets
      if( jet_selector.isJetBFlavor(jet) ){
        jets_type.push_back(*jet);
      }

      // print the kinematics of each jet in the event
      // std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << " phi=" << jet->phi() << " m=" << jet->m() << std::endl;

      jets_raw.push_back(*jet);

      // make kinematic cuts for jets
      if( jet_selector.isJetGood(calibratedjet) ){
	jets_kin_calib.push_back(*calibratedjet);
	// make b type requirement for jets
        if( jet_selector.isJetBFlavor(calibratedjet) ){
          jets_comb_calib.push_back(*calibratedjet);
        }      
      }
   
      // make b type requirement for jets
      if( jet_selector.isJetBFlavor(calibratedjet) ){
	jets_type_calib.push_back(*calibratedjet);
      }

      // check for em jets
      /*if( calibratedjet->em_frac()>0.8 ){
	jets_em.push_back(*calibratedjet);
      }*/

      // print the kinematics of each jet in the event
      // std::cout << "Jet : pt=" << calibratedjet->pt() << "  eta=" << calibratedjet->eta() << "  phi=" << calibratedjet->phi() << "  m=" << calibratedjet->m() << std::endl;

      jets_raw_calib.push_back(*calibratedjet);

      // cleanup
      delete calibratedjet;

    }

    // fill the analysis histograms accordingly
    h_njets_raw->Fill( jets_raw.size() );
    h_njets_raw_calib->Fill( jets_raw_calib.size() );
    h_njets_kin->Fill( jets_kin.size() );
    h_njets_kin_calib->Fill( jets_kin_calib.size() );
    h_njets_type->Fill( jets_type.size() );
    h_njets_type_calib->Fill( jets_type_calib.size() );
    h_njets_comb->Fill( jets_comb.size() );
    h_njets_comb_calib->Fill( jets_comb_calib.size() );

    if( jets_raw.size()>=2 ){
      h_mjj_raw->Fill( (jets_raw.at(0).p4()+jets_raw.at(1).p4()).M()/1000. );
      if( jets_raw.size()>=3 ){
	mjj_sublead_subsublead->Fill( (jets_raw.at(1).p4()+jets_raw.at(2).p4()).M()/1000. );
        if( jets_raw.size()>=4 ){
	  mjj_subsublead_subsubsublead->Fill( (jets_raw.at(2).p4()+jets_raw.at(3).p4()).M()/1000. );
	}  
      }
    }

    if( jets_raw_calib.size()>=2 ){
      h_mjj_raw_calib->Fill( (jets_raw_calib.at(0).p4()+jets_raw_calib.at(1).p4()).M()/1000. );
    }

    if( jets_kin.size()>=2 ){
      h_mjj_kin->Fill( (jets_kin.at(0).p4()+jets_kin.at(1).p4()).M()/1000. );
    }

    if( jets_kin_calib.size()>=2 ){
      h_mjj_kin_calib->Fill( (jets_kin_calib.at(0).p4()+jets_kin_calib.at(1).p4()).M()/1000. );
    }

    if( jets_type.size()>=2 ){
      h_mjj_type->Fill( (jets_type.at(0).p4()+jets_type.at(1).p4()).M()/1000. );
    }

    if( jets_type_calib.size()>=2 ){
      h_mjj_type_calib->Fill( (jets_type_calib.at(0).p4()+jets_type_calib.at(1).p4()).M()/1000. );
    }

    if( jets_comb.size()>=2 ){
      h_mjj_comb->Fill( (jets_comb.at(0).p4()+jets_comb.at(1).p4()).M()/1000. );
    }

    if( jets_comb_calib.size()>=2 ){
      h_mjj_comb_calib->Fill( (jets_comb_calib.at(0).p4()+jets_comb_calib.at(1).p4()).M()/1000. );
    }

    // counter for the number of events analyzed thus far
    count += 1;
  }

  if(argc >= 3) outputFilePath = argv[2];

  // open TFile to store the analysis histogram output
  TFile *fout = new TFile(outputFilePath,"RECREATE");

  h_njets_raw->Write();
  h_njets_raw_calib->Write();
  h_njets_kin->Write();
  h_njets_kin_calib->Write();
  h_njets_type->Write();
  h_njets_type_calib->Write();
  h_njets_comb->Write();
  h_njets_comb_calib->Write();

  h_mjj_raw->Write();
  h_mjj_raw_calib->Write();
  h_mjj_kin->Write();
  h_mjj_kin_calib->Write();
  h_mjj_type->Write();
  h_mjj_type_calib->Write();
  h_mjj_comb->Write();
  h_mjj_comb_calib->Write();

  mjj_sublead_subsublead->Write();
  mjj_subsublead_subsubsublead->Write();

  fout->Close();

  std::cout<<"Event Count: "<<eventcount<<"	"<<"Jet Count: "<<jetcount<<std::endl;

  // exit from the main function cleanly
  return 0;
}
